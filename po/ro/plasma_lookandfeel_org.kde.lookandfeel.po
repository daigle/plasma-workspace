# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# Sergiu Bivol <sergiu@cip.md>, 2020, 2021, 2022, 2023, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-30 00:39+0000\n"
"PO-Revision-Date: 2024-02-24 18:37+0000\n"
"Last-Translator: Sergiu Bivol <sergiu@cip.md>\n"
"Language-Team: Romanian <kde-i18n-ro@kde.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../sddm-theme/KeyboardButton.qml:19
#, kde-format
msgid "Keyboard Layout: %1"
msgstr "Aranjament de tastatură: %1"

#: ../sddm-theme/Login.qml:85
#, kde-format
msgid "Username"
msgstr "Nume de utilizator"

#: ../sddm-theme/Login.qml:102 contents/lockscreen/MainBlock.qml:64
#, kde-format
msgid "Password"
msgstr "Parolă"

#: ../sddm-theme/Login.qml:144 ../sddm-theme/Login.qml:150
#, kde-format
msgid "Log In"
msgstr "Autentificare"

#: ../sddm-theme/Main.qml:200 contents/lockscreen/LockScreenUi.qml:276
#, kde-format
msgid "Caps Lock is on"
msgstr "Caps Lock e activ"

#: ../sddm-theme/Main.qml:212 ../sddm-theme/Main.qml:356
#: contents/logout/Logout.qml:200
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Adormire"

#: ../sddm-theme/Main.qml:219 ../sddm-theme/Main.qml:363
#: contents/logout/Logout.qml:225 contents/logout/Logout.qml:238
#, kde-format
msgid "Restart"
msgstr "Repornire"

#: ../sddm-theme/Main.qml:226 ../sddm-theme/Main.qml:370
#: contents/logout/Logout.qml:251
#, kde-format
msgid "Shut Down"
msgstr "Deconectare"

#: ../sddm-theme/Main.qml:233
#, kde-format
msgctxt "For switching to a username and password prompt"
msgid "Other…"
msgstr "Altul…"

#: ../sddm-theme/Main.qml:342
#, kde-format
msgid "Type in Username and Password"
msgstr "Tastați numele de utilizator și parola"

#: ../sddm-theme/Main.qml:377
#, kde-format
msgid "List Users"
msgstr "Enumeră utilizatorii"

#: ../sddm-theme/Main.qml:452 contents/lockscreen/LockScreenUi.qml:363
#, kde-format
msgctxt "Button to show/hide virtual keyboard"
msgid "Virtual Keyboard"
msgstr "Tastatură virtuală"

#: ../sddm-theme/Main.qml:526
#, kde-format
msgid "Login Failed"
msgstr "Autentificare eșuată"

#: ../sddm-theme/SessionButton.qml:18
#, kde-format
msgid "Desktop Session: %1"
msgstr "Sesiune de birou: %1"

#: contents/lockscreen/config.qml:19
#, kde-format
msgctxt "@title: group"
msgid "Clock:"
msgstr "Ceas:"

#: contents/lockscreen/config.qml:22
#, kde-format
msgctxt "@option:check"
msgid "Keep visible when unlocking prompt disappears"
msgstr "Păstrează vizibil când dispare dialogul de deblocare"

#: contents/lockscreen/config.qml:33
#, kde-format
msgctxt "@title: group"
msgid "Media controls:"
msgstr "Controale multimedia:"

#: contents/lockscreen/config.qml:36
#, kde-format
msgctxt "@option:check"
msgid "Show under unlocking prompt"
msgstr "Arată sub dialogul de deblocare"

#: contents/lockscreen/LockScreenUi.qml:50
#, kde-format
msgid "Unlocking failed"
msgstr "Deblocare eșuată"

#: contents/lockscreen/LockScreenUi.qml:290
#, kde-format
msgid "Sleep"
msgstr "Adormire"

#: contents/lockscreen/LockScreenUi.qml:296 contents/logout/Logout.qml:210
#, kde-format
msgid "Hibernate"
msgstr "Hibernează"

#: contents/lockscreen/LockScreenUi.qml:302
#, kde-format
msgid "Switch User"
msgstr "Schimbă utilizatorul"

#: contents/lockscreen/LockScreenUi.qml:387
#, kde-format
msgctxt "Button to change keyboard layout"
msgid "Switch layout"
msgstr "Schimbă aranjamentul"

#: contents/lockscreen/MainBlock.qml:107
#: contents/lockscreen/NoPasswordUnlock.qml:17
#, kde-format
msgid "Unlock"
msgstr "Deblochează"

#: contents/lockscreen/MainBlock.qml:156
#, kde-format
msgid "(or scan your fingerprint on the reader)"
msgstr "(sau scanați amprenta pe cititor)"

#: contents/lockscreen/MainBlock.qml:160
#, kde-format
msgid "(or scan your smartcard)"
msgstr "(sau scanați smartcard-ul)"

#: contents/lockscreen/MediaControls.qml:59
#, kde-format
msgid "No title"
msgstr "Fără titlu"

#: contents/lockscreen/MediaControls.qml:60
#, kde-format
msgid "No media playing"
msgstr "Nimic în redare"

#: contents/lockscreen/MediaControls.qml:88
#, kde-format
msgid "Previous track"
msgstr "Pista precedentă"

#: contents/lockscreen/MediaControls.qml:100
#, kde-format
msgid "Play or Pause media"
msgstr "Redă sau întrerupe multimedia"

#: contents/lockscreen/MediaControls.qml:110
#, kde-format
msgid "Next track"
msgstr "Pista următoare"

#: contents/logout/Logout.qml:151
#, kde-format
msgid "Installing software updates and restarting in 1 second"
msgid_plural "Installing software updates and restarting in %1 seconds"
msgstr[0] ""
"Se instalează actualizări de programe și se repornește în o secundă."
msgstr[1] ""
"Se instalează actualizări de programe și se repornește în %1 secunde."
msgstr[2] ""
"Se instalează actualizări de programe și se repornește în %1 de secunde."

#: contents/logout/Logout.qml:152
#, kde-format
msgid "Restarting in 1 second"
msgid_plural "Restarting in %1 seconds"
msgstr[0] "Se repornește în 1 secundă"
msgstr[1] "Se repornește în %1 secunde"
msgstr[2] "Se repornește în %1 de secunde"

#: contents/logout/Logout.qml:154
#, kde-format
msgid "Logging out in 1 second"
msgid_plural "Logging out in %1 seconds"
msgstr[0] "Se iese în 1 secundă"
msgstr[1] "Se iese în %1 secunde"
msgstr[2] "Se iese în %1 de secunde"

#: contents/logout/Logout.qml:157
#, kde-format
msgid "Shutting down in 1 second"
msgid_plural "Shutting down in %1 seconds"
msgstr[0] "Se deconectează în 1 secundă"
msgstr[1] "Se deconectează în %1 secunde"
msgstr[2] "Se deconectează în %1 de secunde"

#: contents/logout/Logout.qml:172
#, kde-format
msgid ""
"One other user is currently logged in. If the computer is shut down or "
"restarted, that user may lose work."
msgid_plural ""
"%1 other users are currently logged in. If the computer is shut down or "
"restarted, those users may lose work."
msgstr[0] ""
"Un alt utilizator e autentificat momentan. Dacă calculatorul e deconectat "
"sau repornit, acel utilizator poate pierde lucru."
msgstr[1] ""
"%1 alți utilizatori sunt autentificați momentan. Dacă calculatorul e "
"deconectat sau repornit, acei utilizatori pot pierde lucru."
msgstr[2] ""
"%1 de alți utilizatori sunt autentificați momentan. Dacă calculatorul e "
"deconectat sau repornit, acei utilizatori pot pierde lucru."

#: contents/logout/Logout.qml:187
#, kde-format
msgid "When restarted, the computer will enter the firmware setup screen."
msgstr ""
"La repornire, calculatorul va intra în ecranul de configurare firmware."

#: contents/logout/Logout.qml:201
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep Now"
msgstr "Dormi acum"

#: contents/logout/Logout.qml:211
#, kde-format
msgid "Hibernate Now"
msgstr "Hibernează acum"

#: contents/logout/Logout.qml:222
#, kde-format
msgctxt "@action:button Keep short"
msgid "Install Updates & Restart"
msgstr "Instalează actualizări și repornește"

#: contents/logout/Logout.qml:223
#, kde-format
msgctxt "@action:button Keep short"
msgid "Install Updates & Restart Now"
msgstr "Instalează actualizări și repornește acum"

#: contents/logout/Logout.qml:226 contents/logout/Logout.qml:239
#, kde-format
msgid "Restart Now"
msgstr "Repornește acum"

#: contents/logout/Logout.qml:252
#, kde-format
msgid "Shut Down Now"
msgstr "Oprește acum"

#: contents/logout/Logout.qml:262
#, kde-format
msgid "Log Out"
msgstr "Ieși din sistem"

#: contents/logout/Logout.qml:263
#, kde-format
msgid "Log Out Now"
msgstr "Ieși din sistem acum"

#: contents/logout/Logout.qml:273
#, kde-format
msgid "Cancel"
msgstr "Renunță"

#: contents/osd/OsdItem.qml:32
#, kde-format
msgctxt "Percentage value"
msgid "%1%"
msgstr "%1%"

#: contents/splash/Splash.qml:79
#, kde-format
msgctxt ""
"This is the first text the user sees while starting in the splash screen, "
"should be translated as something short, is a form that can be seen on a "
"product. Plasma is the project name so shouldn't be translated."
msgid "Plasma made by KDE"
msgstr "Plasma, de către KDE"

#~ msgid "OK"
#~ msgstr "Bine"

#~ msgid "Switch to This Session"
#~ msgstr "Comută la această sesiune"

#~ msgid "Start New Session"
#~ msgstr "Începe o sesiune nouă"

#~ msgid "Back"
#~ msgstr "Înapoi"

#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgid "Battery at %1%"
#~ msgstr "Acumulator la %1%"

#~ msgctxt "Nobody logged in on that session"
#~ msgid "Unused"
#~ msgstr "Neutilizată"

#~ msgctxt "User logged in on console number"
#~ msgid "TTY %1"
#~ msgstr "TTY %1"

#~ msgctxt "User logged in on console (X display number)"
#~ msgid "on TTY %1 (Display %2)"
#~ msgstr "pe TTY %1 (ecranul %2)"

#~ msgctxt "Username (location)"
#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"

#~ msgid "100%"
#~ msgstr "100%"

#~ msgid "Configure"
#~ msgstr "Configurează"

#~ msgid "Configure KRunner Behavior"
#~ msgstr "Configurează comportamentul KRunner"

#~ msgid "Configure KRunner…"
#~ msgstr "Configurează KRunner…"

#~ msgctxt "Textfield placeholder text, query specific KRunner"
#~ msgid "Search '%1'…"
#~ msgstr "Caută „%1”…"

#~ msgctxt "Textfield placeholder text"
#~ msgid "Search…"
#~ msgstr "Caută…"

#~ msgid "Show Usage Help"
#~ msgstr "Arată ajutorul despre utilizare"

#~ msgid "Pin"
#~ msgstr "Fixează"

#~ msgid "Pin Search"
#~ msgstr "Fixează căutarea"

#~ msgid "Keep Open"
#~ msgstr "Ține deschis"

#~ msgid "Recent Queries"
#~ msgstr "Interogări recente"

#~ msgid "Remove"
#~ msgstr "Elimină"

#~ msgid "in category recent queries"
#~ msgstr "în categoria interogări recente"

#~ msgctxt "verb, to show something"
#~ msgid "Show:"
#~ msgstr "Arată:"

#~ msgid "Configure Search Plugins"
#~ msgstr "Configurează extensii de căutare"

#~ msgid "Close"
#~ msgstr "Închide"
