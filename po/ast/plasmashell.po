# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# SPDX-FileCopyrightText: 2023 Enol P. <enolp@softastur.org>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-22 00:39+0000\n"
"PO-Revision-Date: 2023-12-14 22:48+0100\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: Assamese <alministradores@softastur.org>\n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Softastur"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "alministradores@softastur.org"

#: currentcontainmentactionsmodel.cpp:206
#, kde-format
msgid "Configure Mouse Actions Plugin"
msgstr ""

#: desktopview.cpp:210
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Dev"
msgstr "KDE Plasma 6.0 Dev"

#: desktopview.cpp:213
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Alpha"
msgstr "KDE Plasma 6.0 Alpha"

#: desktopview.cpp:216
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Beta 1"
msgstr "KDE Plasma 6.0 Beta 1"

#: desktopview.cpp:219
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Beta 2"
msgstr "KDE Plasma 6.0 Beta 2"

#: desktopview.cpp:222
#, kde-format
msgctxt "@label %1 is the Plasma version, RC meaning Release Candidate"
msgid "KDE Plasma 6.0 RC1"
msgstr "KDE Plasma 6.0 RC1"

#: desktopview.cpp:225
#, kde-format
msgctxt "@label %1 is the Plasma version, RC meaning Release Candidate"
msgid "KDE Plasma 6.0 RC2"
msgstr "KDE Plasma 6.0 RC2"

#: desktopview.cpp:251
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1 Dev"
msgstr "KDE Plasma %1 Dev"

#: desktopview.cpp:255
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1 Beta"
msgstr "KDE Plasma %1 Beta"

#: desktopview.cpp:258
#, kde-format
msgctxt "@label %1 is the Plasma version, %2 is the beta release number"
msgid "KDE Plasma %1 Beta %2"
msgstr "KDE Plasma %1 Beta %2"

#: desktopview.cpp:262
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1"
msgstr "KDE Plasma %1"

#: desktopview.cpp:268
#, kde-format
msgctxt "@info:usagetip"
msgid "Visit bugs.kde.org to report issues"
msgstr "Visita bugs.kde.org pa informar de problemes"

#: main.cpp:85
#, kde-format
msgid "Plasma Shell"
msgstr ""

#: main.cpp:97
#, kde-format
msgid "Enable QML Javascript debugger"
msgstr ""

#: main.cpp:100
#, kde-format
msgid "Do not restart plasma-shell automatically after a crash"
msgstr ""

#: main.cpp:103
#, kde-format
msgid "Force loading the given shell plugin"
msgstr ""

#: main.cpp:107
#, kde-format
msgid "Replace an existing instance"
msgstr ""

#: main.cpp:110
#, kde-format
msgid ""
"Enables test mode and specifies the layout javascript file to set up the "
"testing environment"
msgstr ""

#: main.cpp:111
#, kde-format
msgid "file"
msgstr ""

#: main.cpp:115
#, kde-format
msgid "Lists the available options for user feedback"
msgstr ""

#: main.cpp:197
#, kde-format
msgid "Plasma Failed To Start"
msgstr ""

#: main.cpp:198
#, kde-format
msgid ""
"Plasma is unable to start as it could not correctly use OpenGL 2 or software "
"fallback\n"
"Please check that your graphic drivers are set up correctly."
msgstr ""

#: osd.cpp:53
#, kde-format
msgctxt "OSD informing that the system is muted, keep short"
msgid "Audio Muted"
msgstr ""

#: osd.cpp:75
#, kde-format
msgctxt "OSD informing that the microphone is muted, keep short"
msgid "Microphone Muted"
msgstr ""

#: osd.cpp:91
#, kde-format
msgctxt "OSD informing that some media app is muted, eg. Amarok Muted"
msgid "%1 Muted"
msgstr ""

#: osd.cpp:113
#, kde-format
msgctxt "touchpad was enabled, keep short"
msgid "Touchpad On"
msgstr ""

#: osd.cpp:115
#, kde-format
msgctxt "touchpad was disabled, keep short"
msgid "Touchpad Off"
msgstr ""

#: osd.cpp:122
#, kde-format
msgctxt "wireless lan was enabled, keep short"
msgid "Wifi On"
msgstr ""

#: osd.cpp:124
#, kde-format
msgctxt "wireless lan was disabled, keep short"
msgid "Wifi Off"
msgstr ""

#: osd.cpp:131
#, kde-format
msgctxt "Bluetooth was enabled, keep short"
msgid "Bluetooth On"
msgstr ""

#: osd.cpp:133
#, kde-format
msgctxt "Bluetooth was disabled, keep short"
msgid "Bluetooth Off"
msgstr ""

#: osd.cpp:140
#, kde-format
msgctxt "mobile internet was enabled, keep short"
msgid "Mobile Internet On"
msgstr ""

#: osd.cpp:142
#, kde-format
msgctxt "mobile internet was disabled, keep short"
msgid "Mobile Internet Off"
msgstr ""

#: osd.cpp:150
#, kde-format
msgctxt ""
"on screen keyboard was enabled because physical keyboard got unplugged, keep "
"short"
msgid "On-Screen Keyboard Activated"
msgstr ""

#: osd.cpp:153
#, kde-format
msgctxt ""
"on screen keyboard was disabled because physical keyboard was plugged in, "
"keep short"
msgid "On-Screen Keyboard Deactivated"
msgstr ""

#: panelview.cpp:1027
#, kde-format
msgctxt "@action:inmenu"
msgid "Hide Panel Configuration"
msgstr ""

#: panelview.cpp:1029
#, kde-format
msgctxt "@action:inmenu"
msgid "Show Panel Configuration"
msgstr ""

#: shellcontainmentconfig.cpp:105
#, kde-format
msgid "Internal Screen on %1"
msgstr ""

#: shellcontainmentconfig.cpp:109
#, kde-format
msgctxt "Screen manufacturer and model on connector"
msgid "%1 %2 on %3"
msgstr ""

#: shellcontainmentconfig.cpp:132
#, kde-format
msgid "Disconnected Screen %1"
msgstr ""

#: shellcorona.cpp:205 shellcorona.cpp:207
#, kde-format
msgid "Show Desktop"
msgstr ""

#: shellcorona.cpp:207
#, kde-format
msgid "Hide Desktop"
msgstr ""

#: shellcorona.cpp:223
#, kde-format
msgid "Show Activity Switcher"
msgstr "Amosar el conmutador d'actividaes"

#: shellcorona.cpp:234
#, kde-format
msgid "Stop Current Activity"
msgstr ""

#: shellcorona.cpp:242
#, kde-format
msgid "Switch to Previous Activity"
msgstr ""

#: shellcorona.cpp:250
#, kde-format
msgid "Switch to Next Activity"
msgstr ""

#: shellcorona.cpp:265
#, kde-format
msgid "Activate Task Manager Entry %1"
msgstr ""

#: shellcorona.cpp:292
#, kde-format
msgid "Manage Desktops And Panels..."
msgstr ""

#: shellcorona.cpp:314
#, kde-format
msgid "Move keyboard focus between panels"
msgstr ""

#: shellcorona.cpp:2064
#, kde-format
msgid "Add Panel"
msgstr "Amestar un panel"

#: shellcorona.cpp:2106
#, kde-format
msgctxt "Creates an empty containment (%1 is the containment name)"
msgid "Empty %1"
msgstr ""

#: softwarerendernotifier.cpp:27
#, kde-format
msgid "Software Renderer In Use"
msgstr ""

#: softwarerendernotifier.cpp:28
#, kde-format
msgctxt "Tooltip telling user their GL drivers are broken"
msgid "Software Renderer In Use"
msgstr ""

#: softwarerendernotifier.cpp:29
#, kde-format
msgctxt "Tooltip telling user their GL drivers are broken"
msgid "Rendering may be degraded"
msgstr ""

#: softwarerendernotifier.cpp:39
#, kde-format
msgid "Never show again"
msgstr ""

#: userfeedback.cpp:36
#, kde-format
msgid "Panel Count"
msgstr ""

#: userfeedback.cpp:40
#, kde-format
msgid "Counts the panels"
msgstr ""
