# translation of plasma_applet_devicenotifier.po to Turkish
# translation of plasma_applet_devicenotifier.po to
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Serdar Soytetir <tulliana@gmail.com>, 2007, 2008, 2009, 2011.
# Serdar Soytetir <tulliana@gmail.com>, 2007, 2012.
# H. İbrahim Güngör <ibrahim@pardus.org.tr>, 2010.
# Volkan Gezer <volkangezer@gmail.com>, 2013, 2017, 2021, 2022.
# SPDX-FileCopyrightText: 2022, 2023, 2024 Emir SARI <emir_sari@icloud.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_devicenotifier\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:39+0000\n"
"PO-Revision-Date: 2024-03-18 19:21+0300\n"
"Last-Translator: Emir SARI <emir_sari@icloud.com>\n"
"Language-Team: Turkish <kde-l10n-tr@kde.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 24.02.0\n"
"X-POOTLE-MTIME: 1413130721.000000\n"

#: package/contents/ui/DeviceItem.qml:188
#, kde-format
msgctxt "@info:status Free disk space"
msgid "%1 free of %2"
msgstr "%1/%2 boş"

#: package/contents/ui/DeviceItem.qml:192
#, kde-format
msgctxt ""
"Accessing is a less technical word for Mounting; translation should be short "
"and mean 'Currently mounting this device'"
msgid "Accessing…"
msgstr "Erişiliyor…"

#: package/contents/ui/DeviceItem.qml:195
#, kde-format
msgctxt ""
"Removing is a less technical word for Unmounting; translation should be "
"short and mean 'Currently unmounting this device'"
msgid "Removing…"
msgstr "Kaldırılıyor…"

#: package/contents/ui/DeviceItem.qml:198
#, kde-format
msgid "Don't unplug yet! Files are still being transferred…"
msgstr "Henüz çıkarmayın! Dosyalar hâlâ aktarılıyor…"

#: package/contents/ui/DeviceItem.qml:229
#, kde-format
msgid "Open in File Manager"
msgstr "Dosya Yöneticisinde Aç"

#: package/contents/ui/DeviceItem.qml:232
#, kde-format
msgid "Mount and Open"
msgstr "Bağla ve Aç"

#: package/contents/ui/DeviceItem.qml:234
#, kde-format
msgid "Eject"
msgstr "Çıkar"

#: package/contents/ui/DeviceItem.qml:236
#, kde-format
msgid "Safely remove"
msgstr "Güvenli Kaldır"

#: package/contents/ui/DeviceItem.qml:278
#, kde-format
msgid "Mount"
msgstr "Bağla"

#: package/contents/ui/FullRepresentation.qml:43
#: package/contents/ui/main.qml:225
#, kde-format
msgid "Remove All"
msgstr "Tümünü Kaldır"

#: package/contents/ui/FullRepresentation.qml:44
#, kde-format
msgid "Click to safely remove all devices"
msgstr "Tüm aygıtları güvenle kaldırmak için tıklayın"

#: package/contents/ui/FullRepresentation.qml:186
#, kde-format
msgid "No removable devices attached"
msgstr "Takılı çıkarılabilir aygıt yok"

#: package/contents/ui/FullRepresentation.qml:186
#, kde-format
msgid "No disks available"
msgstr "Kullanılabilir disk yok"

#: package/contents/ui/main.qml:57
#, kde-format
msgid "Most Recent Device"
msgstr "Son Takılan Aygıt"

#: package/contents/ui/main.qml:57
#, kde-format
msgid "No Devices Available"
msgstr "Kullanılabilir Aygıt Yok"

#: package/contents/ui/main.qml:207
#, kde-format
msgctxt "Open auto mounter kcm"
msgid "Configure Removable Devices…"
msgstr "Çıkarılabilir Aygıtları Yapılandır…"

#: package/contents/ui/main.qml:233
#, kde-format
msgid "Removable Devices"
msgstr "Çıkarılabilir Aygıtlar"

#: package/contents/ui/main.qml:248
#, kde-format
msgid "Non Removable Devices"
msgstr "Çıkarılabilir Olmayan Aygıtlar"

#: package/contents/ui/main.qml:263
#, kde-format
msgid "All Devices"
msgstr "Tüm Aygıtlar"

#: package/contents/ui/main.qml:280
#, kde-format
msgid "Show popup when new device is plugged in"
msgstr "Yeni Aygıt Takıldığında Açılır Pencere Göster"

#: plugin/ksolidnotify.cpp:190
#, kde-format
msgid "Device Status"
msgstr "Aygıt Durumu"

#: plugin/ksolidnotify.cpp:190
#, kde-format
msgid "A device can now be safely removed"
msgstr "Aygıt Güvenli Olarak Kaldırılabilir"

#: plugin/ksolidnotify.cpp:191
#, kde-format
msgid "This device can now be safely removed."
msgstr "Bu aygıt artık güvenli olarak kaldırılabilir."

#: plugin/ksolidnotify.cpp:198
#, kde-format
msgid "You are not authorized to mount this device."
msgstr "Bu aygıtı bağlamak için yetkili değilsiniz."

#: plugin/ksolidnotify.cpp:201
#, kde-format
msgctxt "Remove is less technical for unmount"
msgid "You are not authorized to remove this device."
msgstr "Bu aygıtı kaldırmak için yetkili değilsiniz."

#: plugin/ksolidnotify.cpp:204
#, kde-format
msgid "You are not authorized to eject this disc."
msgstr "Bu diski çıkarmak için yetkili değilsiniz."

#: plugin/ksolidnotify.cpp:211
#, kde-format
msgid "Could not mount this device as it is busy."
msgstr "Aygıt meşgul olduğundan bağlanamadı."

#: plugin/ksolidnotify.cpp:242
#, kde-format
msgid "One or more files on this device are open within an application."
msgstr "Bu aygıttaki bir veya daha fazla dosya bir uygulamada açık."

#: plugin/ksolidnotify.cpp:244
#, kde-format
msgid "One or more files on this device are opened in application \"%2\"."
msgid_plural ""
"One or more files on this device are opened in following applications: %2."
msgstr[0] "Bu aygıttaki bir veya daha fazla dosya “%2” uygulamasında açık."
msgstr[1] "Bu aygıttaki bir veya daha fazla dosya şu uygulamalarda açık: %2"

#: plugin/ksolidnotify.cpp:247
#, kde-format
msgctxt "separator in list of apps blocking device unmount"
msgid ", "
msgstr ", "

#: plugin/ksolidnotify.cpp:265
#, kde-format
msgid "Could not mount this device."
msgstr "Bu aygıt bağlanamadı."

#: plugin/ksolidnotify.cpp:268
#, kde-format
msgctxt "Remove is less technical for unmount"
msgid "Could not remove this device."
msgstr "Bu aygıt kaldırılamadı."

#: plugin/ksolidnotify.cpp:271
#, kde-format
msgid "Could not eject this disc."
msgstr "Bu disk çıkarılamadı."
