# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Xəyyam Qocayev <xxmn77@gmail.com>, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:39+0000\n"
"PO-Revision-Date: 2023-02-19 15:13+0400\n"
"Last-Translator: Kheyyam <xxmn77@gmail.com>\n"
"Language-Team: Azerbaijani <kde-i18n-doc@kde.org>\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Xəyyam Qocayev"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "xxmn77@gmail.com"

#: calendar/eventdatadecorator.cpp:51
#, kde-format
msgctxt "Agenda listview section title"
msgid "Holidays"
msgstr "Bayramlar"

#: calendar/eventdatadecorator.cpp:53
#, kde-format
msgctxt "Agenda listview section title"
msgid "Events"
msgstr "Tədbirlər"

#: calendar/eventdatadecorator.cpp:55
#, kde-format
msgctxt "Agenda listview section title"
msgid "Todo"
msgstr "Tapşırıq"

#: calendar/eventdatadecorator.cpp:57
#, kde-format
msgctxt "Means 'Other calendar items'"
msgid "Other"
msgstr "Digər"

#: calendar/qml/DayDelegate.qml:43
#, kde-format
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] "%1 tətbir"
msgstr[1] "%1 tədbir"

#: calendar/qml/DayDelegate.qml:43
#, kde-format
msgid "No events"
msgstr "Heç bir tətbiq yoxdur"

#: calendar/qml/MonthViewHeader.qml:67
#, kde-format
msgctxt "Format: month year"
msgid "%1 %2"
msgstr "%1 %2"

#: calendar/qml/MonthViewHeader.qml:114
#, kde-format
msgid "Days"
msgstr "Günlər"

#: calendar/qml/MonthViewHeader.qml:120
#, kde-format
msgid "Months"
msgstr "Aylar"

#: calendar/qml/MonthViewHeader.qml:126
#, kde-format
msgid "Years"
msgstr "İllər"

#: calendar/qml/MonthViewHeader.qml:164
#, kde-format
msgid "Previous Month"
msgstr "Keçən ay"

#: calendar/qml/MonthViewHeader.qml:166
#, kde-format
msgid "Previous Year"
msgstr "Keçən il"

#: calendar/qml/MonthViewHeader.qml:168
#, kde-format
msgid "Previous Decade"
msgstr "Keçən onillik"

#: calendar/qml/MonthViewHeader.qml:185
#, kde-format
msgctxt "Reset calendar to today"
msgid "Today"
msgstr "Bu gün"

#: calendar/qml/MonthViewHeader.qml:186
#, kde-format
msgid "Reset calendar to today"
msgstr "Bugünkü tarixə keç"

#: calendar/qml/MonthViewHeader.qml:197
#, kde-format
msgid "Next Month"
msgstr "Növbəti ay"

#: calendar/qml/MonthViewHeader.qml:199
#, kde-format
msgid "Next Year"
msgstr "Gələn il"

#: calendar/qml/MonthViewHeader.qml:201
#, kde-format
msgid "Next Decade"
msgstr "Növbəti onillik"

#: calendar/qml/MonthViewHeader.qml:257
#, kde-format
msgid "Keep Open"
msgstr "Açıq saxla"

#: containmentlayoutmanager/qml/BasicAppletContainer.qml:270
#, kde-format
msgid "Configure…"
msgstr "Tənzimlə..."

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:10
#, kde-format
msgid "Screen lock enabled"
msgstr "Ekran kilidləyici aktivdir"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:11
#, kde-format
msgid "Sets whether the screen will be locked after the specified time."
msgstr "Əgər aktivdirsə ekran göstərilən müdətdən sonra kilidlənəcək."

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:16
#, kde-format
msgid "Screen saver timeout"
msgstr "Ekran qoruyucusuna keçid gecikməsi"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:17
#, kde-format
msgid "Sets the minutes after which the screen is locked."
msgstr "Göstərilən dəqiqədən sonra ekran kilidlənəcək"

#: sessionsprivate/sessionsmodel.cpp:235 sessionsprivate/sessionsmodel.cpp:239
#, kde-format
msgid "New Session"
msgstr "Yeni Sesiya"

#: shellprivate/widgetexplorer/kcategorizeditemsviewmodels.cpp:64
#, kde-format
msgid "Filters"
msgstr "Süzgəclər"

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:148
#, kde-format
msgid ""
"This Widget was written for an unknown older version of Plasma and is not "
"compatible with Plasma %1. Please contact the widget's author for an updated "
"version."
msgstr ""

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:152
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with Plasma %2. "
"Please contact the widget's author for an updated version."
msgstr ""

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:156
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with Plasma %2. "
"Please update Plasma in order to use the widget."
msgstr ""

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:161
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with the latest "
"version of Plasma. Please update Plasma in order to use the widget."
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:111
msgctxt "applet category"
msgid "Accessibility"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:112
msgctxt "applet category"
msgid "Application Launchers"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:113
msgctxt "applet category"
msgid "Astronomy"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:114
msgctxt "applet category"
msgid "Date and Time"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:115
msgctxt "applet category"
msgid "Development Tools"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:116
msgctxt "applet category"
msgid "Education"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:117
msgctxt "applet category"
msgid "Environment and Weather"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:118
msgctxt "applet category"
msgid "Examples"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:119
msgctxt "applet category"
msgid "File System"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:120
#, fuzzy
#| msgctxt "NAME OF TRANSLATORS"
#| msgid "Your names"
msgctxt "applet category"
msgid "Fun and Games"
msgstr "Xəyyam Qocayev"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:121
msgctxt "applet category"
msgid "Graphics"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:122
msgctxt "applet category"
msgid "Language"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:123
msgctxt "applet category"
msgid "Mapping"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:124
msgctxt "applet category"
msgid "Miscellaneous"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:125
msgctxt "applet category"
msgid "Multimedia"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:126
msgctxt "applet category"
msgid "Online Services"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:127
msgctxt "applet category"
msgid "Productivity"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:128
msgctxt "applet category"
msgid "System Information"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:129
msgctxt "applet category"
msgid "Utilities"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:130
msgctxt "applet category"
msgid "Windows and Tasks"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:131
msgctxt "applet category"
msgid "Clipboard"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:132
msgctxt "applet category"
msgid "Tasks"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:148
#, kde-format
msgid "All Widgets"
msgstr "Bütün vidjetlər"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:152
#, kde-format
msgid "Running"
msgstr "İşlək"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:158
#, kde-format
msgctxt ""
"@item:inmenu used in the widget filter. Filter widgets that can be un-"
"installed from the system, which are usually installed by the user to a "
"local place."
msgid "Uninstallable"
msgstr "Silinə bilən"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:162
#, kde-format
msgid "Categories:"
msgstr "Kateqoriyalar:"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:232
#, kde-format
msgid "Download New Plasma Widgets"
msgstr "Yeni Plasma vidjetini yükləmək"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:241
#, kde-format
msgid "Install Widget From Local File…"
msgstr "Vidjeti yerli fayldan quraşdırmaq..."

#: shellprivate/widgetexplorer/widgetexplorer.cpp:503
#, kde-format
msgid "Select Plasmoid File"
msgstr "Plasmoid faylını seçmək"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:517
#, kde-format
msgid "Installing the package %1 failed."
msgstr "%1 paketini qurmaq mümkün olmadı."

#: shellprivate/widgetexplorer/widgetexplorer.cpp:517
#, kde-format
msgid "Installation Failure"
msgstr "Quraşdırılma alınmadı"

#~ msgid "&Execute"
#~ msgstr "&İcra etmək"

#~ msgctxt "Toolbar Button to switch to Plasma Scripting Mode"
#~ msgid "Plasma"
#~ msgstr "Plasma"

#~ msgctxt "Toolbar Button to switch to KWin Scripting Mode"
#~ msgid "KWin"
#~ msgstr "KWin"

#~ msgid "Templates"
#~ msgstr "Şablonlar"

#~ msgid "Desktop Shell Scripting Console"
#~ msgstr "Plasma əmrlər toplusu Konsolu"

#~ msgid "Editor"
#~ msgstr "Redaktor"

#~ msgid "Load"
#~ msgstr "Yükləmək"

#~ msgid "Use"
#~ msgstr "İstifadə etmək"

#~ msgid "Output"
#~ msgstr "Çıxış"

#~ msgid "Unable to load script file <b>%1</b>"
#~ msgstr "<b>%1</b> əmr faylını başlatmaq olmur"

#~ msgid "Open Script File"
#~ msgstr "Əmr faylını açmaq"

#~ msgid "Save Script File"
#~ msgstr "Əmr faylını saxlamaq"

#~ msgid "Executing script at %1"
#~ msgstr "%1 də(a) əmr faylını icra etmək"

#~ msgid "Runtime: %1ms"
#~ msgstr "İcra müddəti: %1 msan"

#~ msgid "Download Wallpaper Plugins"
#~ msgstr "Divar Kağızı əlavəsini yükləmək"
