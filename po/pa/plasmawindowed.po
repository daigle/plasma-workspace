# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# SPDX-FileCopyrightText: 2023, 2024 A S Alam <aalam@punlinux.org>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-09 01:39+0000\n"
"PO-Revision-Date: 2024-01-19 17:21-0600\n"
"Last-Translator: A S Alam <aalam@punlinux.org>\n"
"Language-Team: Punjabi <kde-i18n-doc@kde.org>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.4\n"

#: main.cpp:44 plasmawindowedcorona.cpp:103
#, kde-format
msgid "Plasma Windowed"
msgstr "ਪਲਾਜ਼ਮਾ ਵਿੰਡੋਡ"

#: main.cpp:46
#, kde-format
msgid "Enable QML Javascript debugger"
msgstr "QML ਜਾਵਾ-ਸਕਰਿਪਟ ਡੀਬੱਗਰ ਸਮਰੱਥ ਕਰੋ"

#: main.cpp:49 plasmawindowedcorona.cpp:105
#, kde-format
msgid ""
"Makes the plasmoid stay alive in the Notification Area, even when the window "
"is closed."
msgstr ""

#: main.cpp:51
#, kde-format
msgid "Force loading the given shell plugin"
msgstr "ਦਿੱਤੀ ਸ਼ੈਲ ਪਲੱਗਇਨ ਨੂੰ ਲੋਡ ਕਰਨ ਲਈ ਮਜ਼ਬੂਰ ਕਰਦਾ ਹੈ"

#: main.cpp:55 plasmawindowedcorona.cpp:106
#, kde-format
msgid "The applet to open."
msgstr "ਖੋਲ੍ਹਣ ਲਈ ਐਪਲਿਟ ਹੈ।"

#: main.cpp:56 plasmawindowedcorona.cpp:107
#, kde-format
msgid "Arguments to pass to the plasmoid."
msgstr ""

#: plasmawindowedview.cpp:120
#, kde-format
msgid "Close %1"
msgstr "%1 ਬੰਦ ਕਰੋ"
