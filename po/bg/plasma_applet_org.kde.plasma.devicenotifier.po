# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Yasen Pramatarov <yasen@lindeas.com>, 2009, 2010, 2011.
# SPDX-FileCopyrightText: 2022, 2023, 2024 Mincho Kondarev <mkondarev@yahoo.de>
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_devicenotifier\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:39+0000\n"
"PO-Revision-Date: 2024-01-11 21:48+0100\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.4\n"

#: package/contents/ui/DeviceItem.qml:188
#, kde-format
msgctxt "@info:status Free disk space"
msgid "%1 free of %2"
msgstr "%1 свободни от %2"

#: package/contents/ui/DeviceItem.qml:192
#, kde-format
msgctxt ""
"Accessing is a less technical word for Mounting; translation should be short "
"and mean 'Currently mounting this device'"
msgid "Accessing…"
msgstr "Осъществяване на достъп…"

#: package/contents/ui/DeviceItem.qml:195
#, kde-format
msgctxt ""
"Removing is a less technical word for Unmounting; translation should be "
"short and mean 'Currently unmounting this device'"
msgid "Removing…"
msgstr "Премахване…"

#: package/contents/ui/DeviceItem.qml:198
#, kde-format
msgid "Don't unplug yet! Files are still being transferred…"
msgstr "Не отделяйте устройството! Все още се прехвърлят файлове…"

#: package/contents/ui/DeviceItem.qml:229
#, kde-format
msgid "Open in File Manager"
msgstr "Отваряне във Файлов мениджър"

#: package/contents/ui/DeviceItem.qml:232
#, kde-format
msgid "Mount and Open"
msgstr "Монтиране и отваряне"

#: package/contents/ui/DeviceItem.qml:234
#, kde-format
msgid "Eject"
msgstr "Изваждане"

#: package/contents/ui/DeviceItem.qml:236
#, kde-format
msgid "Safely remove"
msgstr "Безопасно отстраняване"

#: package/contents/ui/DeviceItem.qml:278
#, kde-format
msgid "Mount"
msgstr "Монтиране"

#: package/contents/ui/FullRepresentation.qml:43
#: package/contents/ui/main.qml:225
#, kde-format
msgid "Remove All"
msgstr "Премахване на всички"

#: package/contents/ui/FullRepresentation.qml:44
#, kde-format
msgid "Click to safely remove all devices"
msgstr "Щракнете, за да премахнете безопасно всички устройства"

#: package/contents/ui/FullRepresentation.qml:186
#, kde-format
msgid "No removable devices attached"
msgstr "Няма свързани сменяеми устройства"

#: package/contents/ui/FullRepresentation.qml:186
#, kde-format
msgid "No disks available"
msgstr "Няма налични дискове"

#: package/contents/ui/main.qml:57
#, kde-format
msgid "Most Recent Device"
msgstr "Най-ново устройство"

#: package/contents/ui/main.qml:57
#, kde-format
msgid "No Devices Available"
msgstr "Няма налични устройства"

#: package/contents/ui/main.qml:207
#, kde-format
msgctxt "Open auto mounter kcm"
msgid "Configure Removable Devices…"
msgstr "Конфигуриране на сменяеми устройства…"

#: package/contents/ui/main.qml:233
#, kde-format
msgid "Removable Devices"
msgstr "Сменяеми устройства"

#: package/contents/ui/main.qml:248
#, kde-format
msgid "Non Removable Devices"
msgstr "Несменяеми устройства"

#: package/contents/ui/main.qml:263
#, kde-format
msgid "All Devices"
msgstr "Всички устройства"

#: package/contents/ui/main.qml:280
#, kde-format
msgid "Show popup when new device is plugged in"
msgstr "Показване на известие при свързване на ново устройство"

#: plugin/ksolidnotify.cpp:190
#, kde-format
msgid "Device Status"
msgstr "Статус на устройство"

#: plugin/ksolidnotify.cpp:190
#, kde-format
msgid "A device can now be safely removed"
msgstr "Устройството вече може безопасно да бъде премахнато"

#: plugin/ksolidnotify.cpp:191
#, kde-format
msgid "This device can now be safely removed."
msgstr "Това устройство вече може безопасно да бъде премахнато."

#: plugin/ksolidnotify.cpp:198
#, kde-format
msgid "You are not authorized to mount this device."
msgstr "Не сте упълномощени да монтирате това устройство."

#: plugin/ksolidnotify.cpp:201
#, kde-format
msgctxt "Remove is less technical for unmount"
msgid "You are not authorized to remove this device."
msgstr "Не сте упълномощени да премахвате това устройство."

#: plugin/ksolidnotify.cpp:204
#, kde-format
msgid "You are not authorized to eject this disc."
msgstr "Не сте упълномощени да извадите този диск."

#: plugin/ksolidnotify.cpp:211
#, kde-format
msgid "Could not mount this device as it is busy."
msgstr "Неуспех при монтиране на устройството., защото е заето."

#: plugin/ksolidnotify.cpp:242
#, kde-format
msgid "One or more files on this device are open within an application."
msgstr ""
"Един или повече файлове на това устройство са отворени в дадено приложение."

#: plugin/ksolidnotify.cpp:244
#, kde-format
msgid "One or more files on this device are opened in application \"%2\"."
msgid_plural ""
"One or more files on this device are opened in following applications: %2."
msgstr[0] ""
"Един или повече файлове на това устройство се отварят в приложението \"%2\"."
msgstr[1] ""
"Един или повече файлове на това устройство се отварят в следните приложения:"
"%2."

#: plugin/ksolidnotify.cpp:247
#, kde-format
msgctxt "separator in list of apps blocking device unmount"
msgid ", "
msgstr ", "

#: plugin/ksolidnotify.cpp:265
#, kde-format
msgid "Could not mount this device."
msgstr "Неуспех при монтиране на устройството."

#: plugin/ksolidnotify.cpp:268
#, kde-format
msgctxt "Remove is less technical for unmount"
msgid "Could not remove this device."
msgstr "Неуспех при премахване на устройството."

#: plugin/ksolidnotify.cpp:271
#, kde-format
msgid "Could not eject this disc."
msgstr "Неуспех при изваждане на диска."
