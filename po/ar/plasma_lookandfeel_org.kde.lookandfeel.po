# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Safa Alfulaij <safa1996alfulaij@gmail.com>, 2014, 2015, 2016, 2017.
# SPDX-FileCopyrightText: 2021, 2022, 2024 Zayed Al-Saidi <zayed.alsaidi@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-30 00:39+0000\n"
"PO-Revision-Date: 2024-02-10 19:34+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: ar\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 23.08.1\n"

#: ../sddm-theme/KeyboardButton.qml:19
#, kde-format
msgid "Keyboard Layout: %1"
msgstr "تخطيط لوحة المفاتيح: %1"

#: ../sddm-theme/Login.qml:85
#, kde-format
msgid "Username"
msgstr "اسم المستخدم"

#: ../sddm-theme/Login.qml:102 contents/lockscreen/MainBlock.qml:64
#, kde-format
msgid "Password"
msgstr "كلمة السّرّ"

#: ../sddm-theme/Login.qml:144 ../sddm-theme/Login.qml:150
#, kde-format
msgid "Log In"
msgstr "لِج"

#: ../sddm-theme/Main.qml:200 contents/lockscreen/LockScreenUi.qml:276
#, kde-format
msgid "Caps Lock is on"
msgstr "مفتاح Caps Lock مفعّل"

#: ../sddm-theme/Main.qml:212 ../sddm-theme/Main.qml:356
#: contents/logout/Logout.qml:200
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "نَم"

#: ../sddm-theme/Main.qml:219 ../sddm-theme/Main.qml:363
#: contents/logout/Logout.qml:225 contents/logout/Logout.qml:238
#, kde-format
msgid "Restart"
msgstr "أعد التّشغيل"

#: ../sddm-theme/Main.qml:226 ../sddm-theme/Main.qml:370
#: contents/logout/Logout.qml:251
#, kde-format
msgid "Shut Down"
msgstr "أطفئ"

#: ../sddm-theme/Main.qml:233
#, kde-format
msgctxt "For switching to a username and password prompt"
msgid "Other…"
msgstr "الآخرين..."

#: ../sddm-theme/Main.qml:342
#, kde-format
msgid "Type in Username and Password"
msgstr "اكتب اسم المستخدم و كلمة السر"

#: ../sddm-theme/Main.qml:377
#, kde-format
msgid "List Users"
msgstr "اسرد المستخدمين"

#: ../sddm-theme/Main.qml:452 contents/lockscreen/LockScreenUi.qml:363
#, kde-format
msgctxt "Button to show/hide virtual keyboard"
msgid "Virtual Keyboard"
msgstr "لوحة المفاتيح الوهميّة"

#: ../sddm-theme/Main.qml:526
#, kde-format
msgid "Login Failed"
msgstr "فشل الولوج"

#: ../sddm-theme/SessionButton.qml:18
#, kde-format
msgid "Desktop Session: %1"
msgstr "جلسة سطح المكتب: %1"

#: contents/lockscreen/config.qml:19
#, kde-format
msgctxt "@title: group"
msgid "Clock:"
msgstr "الساعة:"

#: contents/lockscreen/config.qml:22
#, kde-format
msgctxt "@option:check"
msgid "Keep visible when unlocking prompt disappears"
msgstr "أبقها مرئية عندما تختفي محث فك القفل"

#: contents/lockscreen/config.qml:33
#, kde-format
msgctxt "@title: group"
msgid "Media controls:"
msgstr "متحكمات الوسائط:"

#: contents/lockscreen/config.qml:36
#, kde-format
msgctxt "@option:check"
msgid "Show under unlocking prompt"
msgstr "اعرضها تحت محث فك القفل"

#: contents/lockscreen/LockScreenUi.qml:50
#, kde-format
msgid "Unlocking failed"
msgstr "فشل فكّ القفل"

#: contents/lockscreen/LockScreenUi.qml:290
#, kde-format
msgid "Sleep"
msgstr "نَم"

#: contents/lockscreen/LockScreenUi.qml:296 contents/logout/Logout.qml:210
#, kde-format
msgid "Hibernate"
msgstr "أسبِت"

#: contents/lockscreen/LockScreenUi.qml:302
#, kde-format
msgid "Switch User"
msgstr "بدّل المستخدم"

#: contents/lockscreen/LockScreenUi.qml:387
#, kde-format
msgctxt "Button to change keyboard layout"
msgid "Switch layout"
msgstr "بدّل التّخطيط"

#: contents/lockscreen/MainBlock.qml:107
#: contents/lockscreen/NoPasswordUnlock.qml:17
#, kde-format
msgid "Unlock"
msgstr "فكّ القفل"

#: contents/lockscreen/MainBlock.qml:156
#, kde-format
msgid "(or scan your fingerprint on the reader)"
msgstr "(أو امسح بصمة إصبعك على القارئ)"

#: contents/lockscreen/MainBlock.qml:160
#, kde-format
msgid "(or scan your smartcard)"
msgstr "(أو امسح بطاقتك الذكية)"

#: contents/lockscreen/MediaControls.qml:59
#, kde-format
msgid "No title"
msgstr "بدون عنوان"

#: contents/lockscreen/MediaControls.qml:60
#, kde-format
msgid "No media playing"
msgstr "لا وسائط تعمل"

#: contents/lockscreen/MediaControls.qml:88
#, kde-format
msgid "Previous track"
msgstr "المقطوعة السابقة"

#: contents/lockscreen/MediaControls.qml:100
#, kde-format
msgid "Play or Pause media"
msgstr "شغّل أو أوقف الوسيط"

#: contents/lockscreen/MediaControls.qml:110
#, kde-format
msgid "Next track"
msgstr "المقطوعة التالية"

#: contents/logout/Logout.qml:151
#, kde-format
msgid "Installing software updates and restarting in 1 second"
msgid_plural "Installing software updates and restarting in %1 seconds"
msgstr[0] "تثبيت تحديثات البرامج وإعادة التشغيل خلال ثانية واحدة"
msgstr[1] "تثبيت تحديثات البرامج وإعادة التشغيل خلال ثانية واحدة"
msgstr[2] "تثبيت تحديثات البرامج وإعادة التشغيل خلال ثانيتين"
msgstr[3] "تثبيت تحديثات البرامج وإعادة التشغيل خلال %1 ثواني"
msgstr[4] "تثبيت تحديثات البرامج وإعادة التشغيل خلال %1 ثانية"
msgstr[5] "تثبيت تحديثات البرامج وإعادة التشغيل خلال %1 ثانية"

#: contents/logout/Logout.qml:152
#, kde-format
msgid "Restarting in 1 second"
msgid_plural "Restarting in %1 seconds"
msgstr[0] "الآن يعيد التشغيل"
msgstr[1] "يعيد التشغيل بعد ثانية"
msgstr[2] "يعيد التشغيل بعد ثانيتين"
msgstr[3] "يعيد التشغيل بعد %1 ثواني"
msgstr[4] "يعيد التشغيل بعد %1 ثانية"
msgstr[5] "يعيد التشغيل بعد %1 ثانية"

#: contents/logout/Logout.qml:154
#, kde-format
msgid "Logging out in 1 second"
msgid_plural "Logging out in %1 seconds"
msgstr[0] "الآن يخرج"
msgstr[1] "يخرج بعد ثانية"
msgstr[2] "يخرج بعد ثانيتين"
msgstr[3] "يخرج بعد %1 ثواني"
msgstr[4] "يخرج بعد %1 ثانية"
msgstr[5] "يخرج بعد %1 ثانية"

#: contents/logout/Logout.qml:157
#, kde-format
msgid "Shutting down in 1 second"
msgid_plural "Shutting down in %1 seconds"
msgstr[0] "الآن يطفئ"
msgstr[1] "يطفئ بعد ثانية"
msgstr[2] "يطفئ بعد ثانيتين"
msgstr[3] "يطفئ بعد %1 ثواني"
msgstr[4] "يطفئ بعد %1 ثانية"
msgstr[5] "يطفئ بعد %1 ثانية"

#: contents/logout/Logout.qml:172
#, kde-format
msgid ""
"One other user is currently logged in. If the computer is shut down or "
"restarted, that user may lose work."
msgid_plural ""
"%1 other users are currently logged in. If the computer is shut down or "
"restarted, those users may lose work."
msgstr[0] ""
"يوجد مستخدم آخر يعمل على هذا الجهاز. إذا أوقف تشغيل الجهاز أو إعادة تشغيله ، "
"فقد يفقد هذا المستخدم عمله."
msgstr[1] ""
"يوجد مستخدم آخر يعمل على هذا الجهاز. إذا أوقف تشغيل الجهاز أو إعادة تشغيله، "
"فقد يفقد هذا المستخدم عمله."
msgstr[2] ""
"يوجد مستخدمين آخرين يعملان على هذا الجهاز. إذا أوقف تشغيل الجهاز أو إعادة "
"تشغيله، فقد يفقد هذان المستخدمان عملهما."
msgstr[3] ""
"يوجد %1 مستخدمين آخرين يعملون على هذا الجهاز. إذا أوقف تشغيل الجهاز أو إعادة "
"تشغيله، فقد يفقد هؤلاء المستخدمون عملهم."
msgstr[4] ""
"يوجد %1 مستخدما آخرا يعملون على هذا الجهاز. إذا أوقف تشغيل الجهاز أو إعادة "
"تشغيله، فقد يفقد هؤلاء المستخدمون عملهم."
msgstr[5] ""
"يوجد %1 مستخدم آخر يعمل على هذا الجهاز. إذا أوقف تشغيل الجهاز أو إعادة "
"تشغيله، فقد يفقد هؤلاء المستخدمين عملهم."

#: contents/logout/Logout.qml:187
#, kde-format
msgid "When restarted, the computer will enter the firmware setup screen."
msgstr "عند إعادة التشغيل ، سيدخل الحاسوب إلى شاشة إعداد البرنامج الثابت."

#: contents/logout/Logout.qml:201
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep Now"
msgstr "نَم الآن"

#: contents/logout/Logout.qml:211
#, kde-format
msgid "Hibernate Now"
msgstr "أسبت الآن"

#: contents/logout/Logout.qml:222
#, kde-format
msgctxt "@action:button Keep short"
msgid "Install Updates & Restart"
msgstr "ثبت التحديثات وأعد التشغيل"

#: contents/logout/Logout.qml:223
#, kde-format
msgctxt "@action:button Keep short"
msgid "Install Updates & Restart Now"
msgstr "ثبت التحديثات وأعد التشغيل الآن"

#: contents/logout/Logout.qml:226 contents/logout/Logout.qml:239
#, kde-format
msgid "Restart Now"
msgstr "أعِد التشغيل الآن"

#: contents/logout/Logout.qml:252
#, kde-format
msgid "Shut Down Now"
msgstr "أطفئ الآن"

#: contents/logout/Logout.qml:262
#, kde-format
msgid "Log Out"
msgstr "اخرج"

#: contents/logout/Logout.qml:263
#, kde-format
msgid "Log Out Now"
msgstr "اخرج الآن"

#: contents/logout/Logout.qml:273
#, kde-format
msgid "Cancel"
msgstr "ألغِ"

#: contents/osd/OsdItem.qml:32
#, kde-format
msgctxt "Percentage value"
msgid "%1%"
msgstr "%1٪"

#: contents/splash/Splash.qml:79
#, kde-format
msgctxt ""
"This is the first text the user sees while starting in the splash screen, "
"should be translated as something short, is a form that can be seen on a "
"product. Plasma is the project name so shouldn't be translated."
msgid "Plasma made by KDE"
msgstr "بلازما من كِيدِي"

#~ msgid "OK"
#~ msgstr "حسنًا"

#~ msgid "Switch to This Session"
#~ msgstr "بدّل إلى هذه الجلسة"

#~ msgid "Start New Session"
#~ msgstr "ابدأ جلسة جديدة"

#~ msgid "Back"
#~ msgstr "عُد"

#~ msgid "%1%"
#~ msgstr "%1٪"

#~ msgid "Battery at %1%"
#~ msgstr "البطارية عند %1%"

#~ msgctxt "Nobody logged in on that session"
#~ msgid "Unused"
#~ msgstr "غير مستخدمة"

#~ msgctxt "User logged in on console number"
#~ msgid "TTY %1"
#~ msgstr "جلسة الطرفيّة %1"

#~ msgctxt "User logged in on console (X display number)"
#~ msgid "on TTY %1 (Display %2)"
#~ msgstr "في جلسة الطّرفيّة %1 (العرض %2)"

#~ msgctxt "Username (location)"
#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"

#~ msgid "100%"
#~ msgstr "100٪"

#~ msgid "Configure"
#~ msgstr "اضبط"

#~ msgid "Configure KRunner Behavior"
#~ msgstr "اضبط سلوك مشغل.ك ..."

#~ msgid "Configure KRunner…"
#~ msgstr "اضبط مشغل.ك ..."

#~ msgctxt "Textfield placeholder text, query specific KRunner"
#~ msgid "Search '%1'…"
#~ msgstr "ابحث '%1'..."

#~ msgctxt "Textfield placeholder text"
#~ msgid "Search…"
#~ msgstr "ابحث..."

#~ msgid "Show Usage Help"
#~ msgstr "اعرض مساعدة الاستعمال"

#~ msgid "Pin"
#~ msgstr "ثبّت"

#~ msgid "Pin Search"
#~ msgstr "ثبّت البحث"

#~ msgid "Keep Open"
#~ msgstr "أبقه مفتوحاً"

#~ msgid "Recent Queries"
#~ msgstr "الاستعلامات الأخيرة"

#~ msgid "Remove"
#~ msgstr "أزل"

#~ msgid "in category recent queries"
#~ msgstr "في تصنيف الاستعلامات الأخيرة"

#~ msgctxt "verb, to show something"
#~ msgid "Show:"
#~ msgstr "اعرض:"

#~ msgid "Configure Search Plugins"
#~ msgstr "اضبط ملحقات البحث"

#~ msgctxt ""
#~ "This is the first text the user sees while starting in the splash screen, "
#~ "should be translated as something short, is a form that can be seen on a "
#~ "product. Plasma is the project name so shouldn't be translated."
#~ msgid "Plasma 25th Anniversary Edition by KDE"
#~ msgstr "إصدار الذكرى السنوية 25 للبلازما من كِيدِي"

#~ msgid "Close"
#~ msgstr "أغلق"

#~ msgid "Suspend"
#~ msgstr "علّق"

#~ msgid "Reboot"
#~ msgstr "أعد الإقلاع"

#~ msgid "Logout"
#~ msgstr "اخرج"

#~ msgid "Different User"
#~ msgstr "مستخدم آخر"

#, fuzzy
#~| msgid "Login as different user"
#~ msgid "Log in as a different user"
#~ msgstr "لِج كمستخدم آخر"

#, fuzzy
#~| msgid "Password"
#~ msgid "Password..."
#~ msgstr "كلمة السّرّ"

#~ msgid "Login"
#~ msgstr "لِج"

#~ msgid "Shutdown"
#~ msgstr "أطفئ"

#~ msgid "Switch"
#~ msgstr "بدّل"

#~ msgid "New Session"
#~ msgstr "جلسة جديدة"

#~ msgid "%1%. Charging"
#~ msgstr "%1%. تشحن"

#~ msgid "Fully charged"
#~ msgstr "مشحونة بالكامل"

#~ msgid "Change Session"
#~ msgstr "غيّر الجلسة"

#~ msgid "Create Session"
#~ msgstr "أنشئ جلسةً"

#~ msgid "Change Session..."
#~ msgstr "غيّر الجلسة..."

#, fuzzy
#~| msgid "%1 (%2)"
#~ msgctxt "Username (logged in on console)"
#~ msgid "%1 (TTY)"
#~ msgstr "%1 (%2)"

#~ msgctxt "Button to restart the computer"
#~ msgid "Reboot"
#~ msgstr "أعد الإقلاع"

#, fuzzy
#~| msgid "Shut down"
#~ msgctxt "Button to shut down the computer"
#~ msgid "Shut down"
#~ msgstr "أطفئ"

#, fuzzy
#~| msgid "Rebooting"
#~ msgctxt "Dialog heading, confirm reboot, not a status label"
#~ msgid "Rebooting"
#~ msgstr "يعيد الإقلاع"

#, fuzzy
#~| msgid "%1 (%2)"
#~ msgctxt "Username (on display number)"
#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"
